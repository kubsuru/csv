<?php
/**
 * Class CSVMerge
 * Ищет имена во втором файле и склеивает соответствующие мм данные их с первым
 * Отдельно выводит тех, кого не нашёл в первом файле
 */
class CSVMerge
{
    /**
     * Запись массива в csv (;)
     * @param $array данные
     * @param $fileName имя файла
     */
    private static function writeCSV($array, $fileName)
    {
        $fp = fopen($fileName, 'w');
        foreach ($array as $fields) {
            fputcsv($fp, $fields, ';');
        }
        fclose($fp);
    }

    /**
     * Читает массив из csv (,)
     * @param $fileName имя файлв
     * @return array массив с данными
     */
    private static function readCSV($fileName)
    {
        return array_map('str_getcsv', file($fileName));
    }

    /** Осуществляет поиск данных во втором файле по имени и добавляет их к данным первого файла.
     * Результат сохраняет в файл
     * @param $firstFileName
     * @param $secondFileName
     * @param $resultFileName
     * @param $notFoundFileName
     */
    public static function merge($firstFileName, $secondFileName, $resultFileName, $notFoundFileName)
    {
        $csv1 = self::readCSV($firstFileName);
        $csv2 = self::readCSV($secondFileName);
        $result = Array();
        $notFound = Array();
        foreach ($csv1 as $id => $item) {
            $name = $item[0];
            $result[$name] = array_merge($item, array($id));
        }

        foreach ($csv2 as $line) {
            if (array_key_exists($line[0], $result)) {
                $result[$line[0]] = array_merge($result[$line[0]], $line);
            } else {
                $notFound[] = $line;
            }
        }
        self::writeCSV($result, $resultFileName);
        self::writeCSV($notFound, $notFoundFileName);
    }
}

CSVMerge::merge('1.csv', '2.csv', 'result_found.csv', 'result_not_found.csv');








